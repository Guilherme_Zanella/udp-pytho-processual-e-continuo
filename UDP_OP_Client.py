import socket

def Main():

    #cria o socket do cliente
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    #define o endereço e a porta do servidor
    destino = ("127.0.0.1", 10000)

    #aguarda o usuário digitar uma mensagem
    message = input("(Press Q to exit)Exemplo de envio '10*+*90'")
    
    while message != 'q' and message:
        #envia a mensagem do usuário para o servidor
        sent = mySocket.sendto(message.encode(), destino)

        #recebe a devolução da mensagem do servidor
        data, server = mySocket.recvfrom(4096)
        print('Received from the server {}: {}'. format(server, data.decode()))

        #aguarda nova mensagem do usuário
        message = input("(Press Q to exit)")
    
    mySocket.close()

if __name__ == '__main__':
    Main()



    