import socket

def Main():
    host = "127.0.0.1"
    port = 10000
    
    # cria o socket UDP do servidor (Internet, Transporte)
    socketUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # configura o IP e a porta que o servidor vai ficar executando
    socketUDP.bind((host,port))
    while True:
        #exemplo de envio do client '10*+*90'
        data, address = socketUDP.recvfrom(4096) #buffer size - bytes
        recebido = data.decode().split("*")
        if recebido[1] == "+":
            rst = int(recebido[0]) + int(recebido[2])
        elif recebido[1] == "-":
            rst = int(recebido[0]) - int(recebido[2])
        elif recebido[1] == "*":
            rst = int(recebido[0]) * int(recebido[2])
        elif recebido[1] == "/":
            rst = int(recebido[0]) / int(recebido[2])
        elif recebido[1] == "%":
            rst = int(recebido[0]) % int(recebido[2])
        else:
            rst = "Unsupported operation"
        sent = socketUDP.sendto(data + " = ".encode() + str(rst).encode() , address)

    socket.close()

if __name__ == '__main__':
    Main()
